class CommandException(Exception):
    pass


class WrongDirectionException(CommandException):
    pass


class WrongCallException(CommandException):
    pass


class FinishStateMashinePillException(Exception):
    pass
