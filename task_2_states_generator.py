import json
from typing import Dict, List
from pydantic import BaseModel

from task_2_main import State


#
# just generating states
#
#
class StateElevatorModel(BaseModel):
    f: int  # curent floor
    s: int = 0  # curent state
    ts: List[int] = []  # targets


class StateElevSystemModel(BaseModel):
    elevators: List[StateElevatorModel]


class StatesGeneranor:
    def dump(self, elevators_model: StateElevSystemModel) -> str:
        values = []

        for elevator in elevators_model.elevators:
            state_copy = elevator.model_copy(deep=True)
            ts = []
            for t in state_copy.ts:
                if (t not in ts):
                    ts.append(t)
            state_copy.ts = ts
            values += list(state_copy.model_dump().values())

        return str(values)

    def command_add(self, state: State, cur_input: str, cmd: str):
        if (cmd not in state.commands[cur_input]):
            state.commands[cur_input].append(cmd)

    def any_translations(self, any_inputs, value) -> Dict[str, str]:
        return dict(zip(any_inputs, [value, ] * len(any_inputs)))

    def generate(self, floors) -> Dict[str, State]:
        states: Dict[str, State]
        states = {}
        all_inputs = [str((0, 0))]
        for i in range(1, floors + 1):
            for j in range(1, floors + 1):
                if (i != j):
                    all_inputs.append(str((i, j)))

        start_state = StateElevSystemModel(
            elevators=[StateElevatorModel(f=1), StateElevatorModel(f=1)])

        new_states_list = self._generate(
            states,
            start_state,
            floors, all_inputs
        )

        while len(new_states_list) > 0:
            new_states_list += self._generate(
                states,
                StateElevSystemModel(**new_states_list.pop(0)),
                floors, all_inputs
            )

        states['START'] = State(
            commands={'START': ['R', 'D']},
            translations={
                'START': self.dump(StateElevSystemModel(
                    elevators=[StateElevatorModel(f=1), StateElevatorModel(f=1)]))
            },
        )

        return states

    def _generate(self, states: Dict[str, State], elevators_model: StateElevSystemModel, floors, inputs: List[str]) -> List[Dict]:
        new_states_list = []

        dump_state_key = self.dump(elevators_model)
        if (dump_state_key in states):
            return new_states_list

        states[dump_state_key] = State(
            translations=self.any_translations(inputs, 'None')
        )
        current_state = states[dump_state_key]

        # read next input if elevator is staying
        for elevator in elevators_model.elevators:
            if (elevator.s == 0):
                for cur_input in inputs:
                    self.command_add(current_state, cur_input, f'R')
                    self.command_add(current_state, cur_input, f'D')
                break

        # update lifts
        new_state_model = self.update_lifts(
            current_state, inputs[0], elevators_model.model_copy(deep=True), floors)
        current_state.translations[inputs[0]] = self.dump(new_state_model)

        if (new_state_model.model_dump() not in new_states_list):
            new_states_list.append(
                new_state_model.model_copy(deep=True).model_dump())

        # fill a translations
        for call_from in range(1, floors + 1):
            for call_to in range(1, floors + 1):
                if (call_from == call_to):
                    continue
                cur_input = str((call_from, call_to))
                dir = 1 if call_to < call_from else 2

                new_state_model = elevators_model.model_copy(deep=True)
                # update lifts
                self.update_lifts(current_state, cur_input,
                                  new_state_model, floors)

                # called on a floor where there is elevator
                is_called = False
                for i, elevator in enumerate(new_state_model.elevators):
                    # just open a doors
                    if (elevator.s == 0 and call_from == elevator.f):
                        elevator.s = 3
                        self.command_add(
                            current_state, cur_input, f'OPEN_{i}')
                        elevator.ts.append(call_to)
                        is_called = True
                        break

                if (not is_called):
                    # called on a floor where there is NO elevator
                    # chose closest
                    closest_elevator = None
                    min_dist = floors
                    for elevator in new_state_model.elevators:
                        if (elevator.s == 0 and abs(elevator.f - call_from) < min_dist):
                            closest_elevator = elevator
                            min_dist = abs(elevator.f - call_from)

                    if (closest_elevator is not None):
                        closest_elevator.s = 1 if closest_elevator.f > call_from else 2
                        closest_elevator.ts.append(call_from)
                        closest_elevator.ts.append(call_to)
                        is_called = True

                current_state.translations[str(
                    (call_from, call_to))] = self.dump(new_state_model)

                if (new_state_model.model_dump() not in new_states_list):
                    new_states_list.append(
                        new_state_model.model_copy(deep=True).model_dump())

        return new_states_list

    def update_lifts(self, current_state: str, cur_input, elevators_model: StateElevSystemModel, floors) -> StateElevSystemModel:
        for i, elevator in enumerate(elevators_model.elevators):
            # close door
            if (elevator.s == 3):
                # stop elevator
                self.command_add(
                    current_state, cur_input, f'CLOSE_{i}')
                if (len(elevator.ts) > 0):
                    elevator.s = 1 if elevator.f > elevator.ts[0] else 2
                else:
                    elevator.s = 0
            # move lifts
            elif (elevator.s == 1):
                self.command_add(current_state, cur_input, f'DOWN_{i}')
                if (elevator.f > 1):
                    elevator.f -= 1
                if (elevator.f in elevator.ts):
                    self.command_add(
                        current_state, cur_input, f'OPEN_{i}')
                    elevator.ts.remove(elevator.f)
                    elevator.s = 3
            elif (elevator.s == 2):
                self.command_add(current_state, cur_input, f'UP_{i}')
                if (elevator.f < floors):
                    elevator.f += 1
                if (elevator.f in elevator.ts):
                    self.command_add(
                        current_state, cur_input, f'OPEN_{i}')
                    elevator.ts.remove(elevator.f)
                    elevator.s = 3
        return elevators_model


#
# test
#
#
generator = StatesGeneranor()

MAX_FLOOR = 4

states = generator.generate(floors=MAX_FLOOR)

for k, v in states.items():
    v.translations = dict(sorted(v.translations.items(), key=lambda x: x[0]))
    v.commands = dict(sorted(v.commands.items(), key=lambda x: x[0]))

    print(f'{k}:\n\tcommands:')
    for trans_k, trans_v in v.commands.items():
        print(f'\t\t{trans_k}: {trans_v}')

    print(f'\n\ttranslations:')
    for trans_k, trans_v in v.translations.items():
        print(f'\t\t{trans_k}: {trans_v}')

with open('states.json', 'w') as file:
    file.write(json.dumps(
        dict([(state_id, value.model_dump()) for state_id, value in states.items()])))
