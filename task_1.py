import time
from enum import Enum
from typing import Dict, Any


# Task 1
# develop a finite state machine for a crossroad with 4 traffic lights


class SimpleStateMachine:
    """
        Gets a transition table and changes a state by action geted in method 'translate'
    """

    _state_table: Dict[Any, Dict[int, Any]]  # transition table
    _cur_state: Any

    def __init__(self, state_table: Dict[Any, Dict[Any, Any]], start_state: Any):
        self._state_table = state_table
        self._cur_state = start_state

    def translate(self, action: Any) -> Any:
        new_state = self._state_table[self._cur_state][action]
        self._cur_state = new_state
        return self.cur_state

    @property
    def cur_state(self) -> Any:
        return self._cur_state


class SemaphoreStates(Enum):
    STOP = 'R'      # R - red color is ON, each other colors are off
    READY = 'RY'    # RY - red and yellow are ON
    GO = 'G'
    GO_NEXT = 'g'    # g - green color is blinking
    WARNING = 'Y'


class CrossroadStates(Enum):
    ACTIVE_1 = (
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO.value,
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO.value,
    )
    ACTIVE_1_NEXT = (
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO_NEXT.value,
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO_NEXT.value,
    )
    WARNING_1 = (
        SemaphoreStates.READY.value,
        SemaphoreStates.WARNING.value,
        SemaphoreStates.READY.value,
        SemaphoreStates.WARNING.value,
    )
    ACTIVE_2 = (
        SemaphoreStates.GO.value,
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO.value,
        SemaphoreStates.STOP.value,
    )
    ACTIVE_2_NEXT = (
        SemaphoreStates.GO_NEXT.value,
        SemaphoreStates.STOP.value,
        SemaphoreStates.GO_NEXT.value,
        SemaphoreStates.STOP.value,
    )
    WARNING_2 = (
        SemaphoreStates.WARNING.value,
        SemaphoreStates.READY.value,
        SemaphoreStates.WARNING.value,
        SemaphoreStates.READY.value,
    )


# states can be described in JSON file

# transition table
# {current state: {action: next state, action: next state, ... }, ... }
state_table = {
    CrossroadStates.ACTIVE_1.value: {
        0: CrossroadStates.ACTIVE_1.value, 1: CrossroadStates.ACTIVE_1_NEXT.value
    },
    CrossroadStates.ACTIVE_1_NEXT.value: {
        0: CrossroadStates.ACTIVE_1_NEXT.value, 1: CrossroadStates.WARNING_1.value
    },
    CrossroadStates.WARNING_1.value: {
        0: CrossroadStates.WARNING_1.value, 1: CrossroadStates.ACTIVE_2.value
    },
    CrossroadStates.ACTIVE_2.value: {
        0: CrossroadStates.ACTIVE_2.value, 1: CrossroadStates.ACTIVE_2_NEXT.value
    },
    CrossroadStates.ACTIVE_2_NEXT.value: {
        0: CrossroadStates.ACTIVE_2_NEXT.value, 1: CrossroadStates.WARNING_2.value
    },
    CrossroadStates.WARNING_2.value: {
        0: CrossroadStates.WARNING_2.value, 1: CrossroadStates.ACTIVE_1.value
    },
}


#
# some render just for example

stateMashine = SimpleStateMachine(state_table, CrossroadStates.ACTIVE_1.value)

total_time = 0
time_last_step = round(time.time() * 1000)
delta_time = 0
while (True):
    # alghoritm has finished after 5 seconds
    if (total_time > 5_000):
        break

    # every 0.5 seconds change a state
    # this time can be described in an array for each state
    if (delta_time > 500):
        delta_time = 0
        stateMashine.translate(1)
        print(*stateMashine.cur_state)

    now_time_millis = round(time.time() * 1000)
    total_time += now_time_millis - time_last_step
    delta_time += now_time_millis - time_last_step
    time_last_step = now_time_millis

# OUTPUT:
#     R  g  R  g
#     RY Y  RY Y
#     G  R  G  R
#     g  R  g  R
#     Y  RY Y  RY
#     R  G  R  G
#     R  g  R  g
#     RY Y  RY Y
#     ...
