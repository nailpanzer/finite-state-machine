# State machine

![graph.png](./graph.png)

## Tasks
1. Develop a finite state machine for a crossroad with 4 traffic lights.
2. Develop a finite state machine for a system of 2 elevators in a house.

## Elevators
The generated states are in the states.json file.

States are described by several values:
* positions of both elevators;
* state of both elevators (0 - waiting, 1 - moving down, 2 - moving up, 3 - standing and doors open);
* array of targets.

For example, the string "1, 2, [3], 1, 0, []" means that elevator 1 rises from the 1st to the 3rd floor, elevator 2 is waiting on the 1st floor without targets.

The input data consists of the floor on which the elevator was called and the target floor.

For example, input "2, 5" means that elevator is called on 2nd floor to get to 5th floor.

## Author
Student of Omsk State Technical University - Gilmutdinov Nail
