from abc import abstractmethod
from collections import defaultdict
import json
from typing import Any, Dict, List
from pydantic import BaseModel
from exceptions import *


class IStateMachine:
    @abstractmethod
    def translate(self) -> None:
        pass

    @abstractmethod
    def set_state(self, state_id) -> None:
        pass

    @abstractmethod
    def set_input(self, new_input) -> None:
        pass

    @abstractmethod
    def get_state(self) -> Any:
        pass

    @abstractmethod
    def get_input(self) -> Any:
        pass


class ElevatorModel(BaseModel):
    id: int
    max_floor: int
    floor: int = 1
    is_open: bool = False


#
# some commands to controll a elevators
#
#
class Command:
    @abstractmethod
    def execute(self) -> None:
        pass

HISTORY = defaultdict(list)


class CmdMoveUp(Command):
    def __init__(self, elevator:  ElevatorModel) -> None:
        self.elevator = elevator

    def execute(self) -> None:
        if (self.elevator.is_open):
            raise CommandException
        self.elevator.floor += 1
        HISTORY[self.elevator.id].append(f'UP to {self.elevator.floor}')
        if (self.elevator.floor > self.elevator.max_floor):
            raise CommandException


class CmdMoveDown(Command):
    def __init__(self, elevator: ElevatorModel) -> None:
        self.elevator = elevator

    def execute(self) -> None:
        if (self.elevator.is_open):
            raise CommandException
        self.elevator.floor -= 1
        HISTORY[self.elevator.id].append(f'DOWN to {self.elevator.floor}')
        if (self.elevator.floor < 1):
            raise CommandException


class CmdDoorOpen(Command):
    def __init__(self, elevator: ElevatorModel) -> None:
        self.elevator = elevator

    def execute(self) -> None:
        if (self.elevator.is_open):
            raise CommandException
        self.elevator.is_open = True
        HISTORY[self.elevator.id].append(f'OPEN')


class CmdDoorClose(Command):
    def __init__(self, elevator: ElevatorModel) -> None:
        self.elevator = elevator

    def execute(self) -> None:
        if (not self.elevator.is_open):
            raise CommandException
        self.elevator.is_open = False
        HISTORY[self.elevator.id].append(f'CLOSE')


class CmdWrongDirectionException(Command):
    def execute(self) -> None:
        raise WrongDirectionException


class CmdWrongInputException(Command):
    def execute(self) -> None:
        raise WrongCallException


#
# commands to read a next input
#
#
class CmdReadNextInput(Command):  # R
    state_machine: IStateMachine
    inputs: List[Any]

    def __init__(self, state_machine, inputs: List[Any]):
        self.state_machine = state_machine
        self.inputs = inputs

    def execute(self) -> None:
        if (len(self.inputs) == 0):
            raise FinishStateMashinePillException
        new_input = self.inputs.pop(0)
        self.inputs.insert(0, self.state_machine.get_input())
        self.state_machine.set_input(new_input)


class CmdDeleteNextInput(Command):  # D
    state_machine: IStateMachine
    inputs: List[Any]

    def __init__(self, state_machine, inputs: List[Any]):
        self.state_machine = state_machine
        self.inputs = inputs

    def execute(self) -> None:
        if (len(self.inputs) == 0):
            raise FinishStateMashinePillException
        self.inputs.pop(0)


# example for 3 floors
#
# State: (L1, L1_state, L1_targets, L2, L2_state, L2_targets)
#
# L1: 1, 2, 3 - elevator is waiting on floor
# L1_state: 0 - stay, 1 - move down, 2 - move up, 3 - stay with door open
# L1_targets: [] - no targets, [1], [2], [3] - elevator is moving to floor
#
# similarly for elevator 2
# L2: ...
# L2_state: ...
# L2_targets: ...
#
# Input: (call, target)
#
# call: 1, 2, 3 - elevator is called on floor
# target: 1, 2, 3 - button inside a elevator is pressed


class State(BaseModel):
    commands: Dict[str, List[str]] = defaultdict(list)
    translations: Dict[str, str] = {}


class StateMachine(IStateMachine):
    cur_state_id: str
    cur_input: str

    states: Dict[str, State]
    commands: Dict[str, Command]

    def __init__(self, elevators: List[ElevatorModel], states: Dict[str, State], start_state_id: str, inputs: List[str]):
        self.states = states
        self.cur_state_id = start_state_id
        self.cur_input = 'START'
        self.commands = {
            'R': CmdReadNextInput(self, inputs),
            'D': CmdDeleteNextInput(self, inputs)
        }
        for i, elevator in enumerate(elevators):
            self.commands[f'UP_{i}'] = CmdMoveUp(elevator)
            self.commands[f'DOWN_{i}'] = CmdMoveDown(elevator)
            self.commands[f'OPEN_{i}'] = CmdDoorOpen(elevator)
            self.commands[f'CLOSE_{i}'] = CmdDoorClose(elevator)

    # the main method that changes the state of the StateMachine
    def translate(self) -> None:
        cur_commands = self.states[self.cur_state_id].commands[self.cur_input]
        self.cur_state_id = self.states[self.cur_state_id].translations[self.cur_input]

        for cmd_name in cur_commands:
            self.commands[cmd_name].execute()

    def set_state(self, state_id) -> None:
        self.cur_state_id = state_id

    def set_input(self, new_input) -> None:
        self.cur_input = new_input

    def get_state(self) -> Any:
        return self.cur_state

    def get_input(self) -> Any:
        return self.cur_input


# The State Machine was created.
# Now all we need to do is create a finite state machine
# that will control the elevators

#
# load the states
#
#
MAX_FLOOR = 4

states: Dict[str, State]
states = {}

with open('states.json', 'r') as file:
    states = json.loads(file.read())
    for k, v in states.items():
        states[k] = State(**v)


# print(states.keys())

#
# test
#
#
inputs = [
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(4, 1)',
    '(1, 3)',
    '(0, 0)',
    '(2, 4)',
    '(1, 3)',
    '(0, 0)',
    '(2, 4)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(1, 4)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
    '(0, 0)',
]

elevators = [
    ElevatorModel(id=1, max_floor=MAX_FLOOR),
    ElevatorModel(id=2, max_floor=MAX_FLOOR)
]

sm = StateMachine(
    elevators,
    states,
    'START',
    inputs
)

while True:
    try:
        sm.translate()
        print(1, elevators[0])
        print(2, elevators[1])
    except FinishStateMashinePillException:
        break

print('History:')
for k, v in HISTORY.items():
    print(f'\t{k}: {v}')